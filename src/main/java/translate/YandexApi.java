package translate;

import org.apache.http.client.utils.URIBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public final class YandexApi {

    public static final String YANDEX_API_KEY = System.getenv("YANDEX_API_KEY");
    static final String SCHEME = "https";
    static final String HOST = "translate.api.cloud.yandex.net";
    static final String PATH_TRANSLATE = "/translate/v2/translate";
    static final String PATH_DETECT = "translate/v2/detect";

    private YandexApi() {
    }

    private static String request(String input) throws IOException {
        URL url = new URL(input);
        HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
        urlConn.setRequestMethod("POST");
        urlConn.addRequestProperty("User-Agent", "Mozilla");

        InputStream inStream = urlConn.getInputStream();

        String recieved = new BufferedReader(new InputStreamReader(inStream)).readLine();

        inStream.close();
        return recieved;
    }

    public static String[] translate(String text, String sourceLang, String targetLang) {
        URIBuilder builder = new URIBuilder()
                .setScheme(SCHEME)
                .setHost(HOST)
                .setPath(PATH_TRANSLATE)
                .setParameter("key", YANDEX_API_KEY)
                .setParameter("text", text)
                .setParameter("lang", sourceLang + "-" + targetLang);

        try {
            String response = request(builder.toString());
            JSONObject jsonObject = new JSONObject(response);
            JSONArray arrJson = jsonObject.getJSONArray("text");
            String[] arr = new String[arrJson.length()];
            for (int i = 0; i < arrJson.length(); i++)
                arr[i] = arrJson.getString(i);
            return arr;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String[0];
    }

    public static String detectLanguage(String text) {
        URIBuilder builder = new URIBuilder()
                .setScheme(SCHEME)
                .setHost(HOST)
                .setPath(PATH_DETECT)
                .setParameter("key", YANDEX_API_KEY)
                .setParameter("text", text);

        try {
            String response = request(builder.toString());
            JSONObject jsonObject = new JSONObject(response);
            return jsonObject.getString("lang");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }
}

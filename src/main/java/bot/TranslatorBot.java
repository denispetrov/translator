package bot;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import translate.YandexApi;

public class TranslatorBot extends TelegramLongPollingBot {

    private static final String BOT_NAME = "TranslateMePlzBot";
    private static final String BOT_TOKEN = System.getenv("BOT_TOKEN");

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {

            String[] textForSend;
            String currentLanguage = YandexApi.detectLanguage(update.getMessage().getText());
            if (currentLanguage.isEmpty()) currentLanguage = "en";

            if (currentLanguage.equalsIgnoreCase("ru")) {
                textForSend = YandexApi.translate(update.getMessage().getText(), "ru", "en");
            } else {
                textForSend = YandexApi.translate(update.getMessage().getText(), currentLanguage, "ru");
            }

            for (String sendText : textForSend) {
                SendMessage message = new SendMessage()
                        .setChatId(update.getMessage().getChatId())
                        .setText(sendText);
                try {
                    execute(message);
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public String getBotUsername() {
        return BOT_NAME;
    }

    @Override
    public String getBotToken() {
        return BOT_TOKEN;
    }
}

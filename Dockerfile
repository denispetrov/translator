FROM zenika/alpine-maven:latest
COPY target/translator-1.0-SNAPSHOT-jar-with-dependencies.jar translator.jar
ENTRYPOINT ["java", "-jar"]
CMD ["translator.jar", "--server.port=8082"]